package database

import (
	"os"
	"time"

	"gopkg.in/mgo.v2"
)

// Session : Database session for use mongodb
var Session *mgo.Session

// Database : Database name for mongodb
var Database string

func init() {
	var (
		err      error
		userName string
		password string
		dbHost   string
	)

	Database = os.Getenv("GIN_DBNAME")
	userName = os.Getenv("GIN_DBUSER")
	password = os.Getenv("GIN_DBPASS")
	dbHost = os.Getenv("GIN_DBHOST")

	mongoDialInfo := &mgo.DialInfo{
		Addrs: []string{
			dbHost,
		},
		Database: Database,
		Username: userName,
		Password: password,
		Timeout:  60 * time.Second,
	}

	Session, err = mgo.DialWithInfo(mongoDialInfo)
	if err != nil {
		panic(err)
	}
}
