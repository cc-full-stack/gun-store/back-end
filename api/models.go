package api

import "gopkg.in/mgo.v2/bson"

// Product : Product resource model
type Product struct {
	ID    bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Title string        `form:"title" json:"title"`
	Image string        `form:"image" json:"image"`
	Price int64         `form:"price" json:"price"`
}

// Coordinate : The coordinate representation
type Coordinate struct {
	Lat float64 `form:"lat" json:"lat"`
	Lon float64 `form:"lon" json:"lon"`
}

// Location : Location resource model
type Location struct {
	ID          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name        string        `form:"name" json:"name"`
	Revenue     float64       `form:"revenue" json:"revenue"`
	Cost        float64       `form:"cost" json:"cost"`
	Currency    string        `form:"currency" json:"currency"`
	Type        string        `form:"type" json:"type"`
	Desciption  string        `form:"description" json:"description"`
	Coordinates Coordinate    `form:"coordinates" json:"coordinates"`
}
