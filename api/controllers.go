package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cc-full-stack/gun-store/back-end/config"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	productsCollection  *mgo.Collection
	locationsCollection *mgo.Collection
)

func init() {
	productsCollection = database.Session.DB(database.Database).C("products")
	locationsCollection = database.Session.DB(database.Database).C("locations")
}

func formatMessage(message string) gin.H {
	return gin.H{"message": message}
}

// GetProducts : Controller for listing all products
func GetProducts(c *gin.Context) {
	var products []*Product

	if err := productsCollection.Find(nil).All(&products); err != nil {
		c.JSON(404, formatMessage(err.Error()))
		return
	}

	if products == nil {
		c.JSON(200, []Product{})
		return
	}

	c.JSON(200, products)
}

// GetLocations : Controller for listing all locations
func GetLocations(c *gin.Context) {
	var locations []*Location

	if err := locationsCollection.Find(nil).All(&locations); err != nil {
		c.JSON(404, formatMessage(err.Error()))
		return
	}

	if locations == nil {
		c.JSON(200, []Location{})
		return
	}

	c.JSON(200, locations)
}

// AddProduct : Controller for adding new product
func AddProduct(c *gin.Context) {
	var json Product

	if err := c.BindJSON(&json); err == nil {
		if err = productsCollection.Insert(&Product{
			bson.NewObjectId(),
			json.Title,
			json.Image,
			json.Price,
		}); err != nil {
			c.JSON(400, formatMessage(err.Error()))
			return
		}

		c.JSON(200, formatMessage("Successfully added."))

	}
}

// AddLocation : Controller for adding new location
func AddLocation(c *gin.Context) {
	var json Location

	if err := c.BindJSON(&json); err == nil {
		if err = locationsCollection.Insert(&Location{
			bson.NewObjectId(),
			json.Name,
			json.Revenue,
			json.Cost,
			json.Currency,
			json.Type,
			json.Desciption,
			json.Coordinates,
		}); err != nil {
			c.JSON(400, formatMessage(err.Error()))
			return
		}

		c.JSON(200, formatMessage("Successfully added."))

	}
}
