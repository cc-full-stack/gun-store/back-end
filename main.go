package main

import (
	"log"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/cc-full-stack/gun-store/back-end/api"
)

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("$PORT must be set")
	}

	r := gin.Default()

	// Use cors
	r.Use(cors.Default())

	r.GET("/products", api.GetProducts)
	r.GET("/locations", api.GetLocations)
	r.POST("/products", api.AddProduct)
	r.POST("/locations", api.AddLocation)

	r.Run(":" + port)
}
